//const URL = "http://localhost/v1/index.php";
const URLN = "http://localhost:3006/api";

export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URLN + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //onsole.log();
    return datos;
}

//Autos
export const AutosCant = async (key) => {
    const cabeceras = {        
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/auto/numero", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const Autos = async (key) => {
    const cabecera = {
                'Accept': 'application/json',
                "X-API-KEY": key
            };
    const datos = await (await fetch(URLN + "/auto", {
        method: "GET",
        headers: cabecera
    })).json();
    
    return datos;
}
export const ObtenerColores = async () => {
    const datos = await (await fetch(URLN + "/auto/colores", {
        method: "GET"
    })).json();
    //console.log(datos);
    return datos;
}
export const GuardarAuto = async (data, key) => {
    const cabecera = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/auto/guardar", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
export const ModificarAutos = async (data, key) => {
    const cabecera = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/auto/modificar", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
//Marcas
export const Marcas = async (key) => {
    const cabeceras = {        
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/marcas/numero", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const ListaMarcas = async (key) => {
    const cabeceras = {        
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/marcas", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}
export const GuardarMarcas = async (data, key) => {
    const headers = {
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/marcas/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}


//Ordenes de mantenimiento
export const ListaOrdenes = async (key) => {
    const cabeceras = {        
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/ordenes", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}


export const ObtenernroOrden = async () => {
    const datos = await (await fetch(URLN + "/ordenes/numerodeOrden'", {
        method: "GET"
    })).json();
    //console.log(datos);
    return datos;
}

export const GuardarOrden = async (data, key) => {
    const cabecera = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/ordenes/guardar", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
export const ModificarOrden = async (data, key) => {
    const cabecera = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URLN + "/ordenes/modificar", {
        method: "PUT",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}