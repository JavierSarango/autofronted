import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { GuardarMarcas } from '../hooks/Conexion';

import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
import { getToken } from '../utilidades/Sessionutil';

const RegistroMarca = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
   
    //acciones
    //submit
    const onSubmit = (data) => {

        var datos = {
            "nombre": data.nombre,
            "pais_origen": data.pais_origen,
            "descripcion": data.descripcion
            
        };
        console.log("datos a enviar", datos)
        GuardarMarcas(datos, getToken()).then((info) => {
            console.log("esta es la info", info.error)
            if (info.error === true) {
                console.log("SI hay error. Lo siento")
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                console.log("No hay error. Suerte campeón!")
                mensajes(info.message);
                navegation('/marca');
            }
        }
        );
    };
  
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de Marcas!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('nombre', { required: true })} className="form-control form-control-user" placeholder="Ingrese el nombre de la marca" />
                                        {errors.nombre && errors.nombre.type === 'required' && <div className='alert alert-danger'>Ingrese un nombre de marca</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el pais de origen" {...register('pais_origen', { required: true })} />
                                        {errors.pais_origen && errors.pais_origen.type === 'required' && <div className='alert alert-danger'>Ingrese el año del vehículo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" {...register('descripcion', { required: true })} className="form-control form-control-user" placeholder="Ingrese descripcion" />
                                        {errors.descripcion && errors.descripcion.type === 'required' && <div className='alert alert-danger'>Ingrese descripcion</div>}
                                    </div>

                                
                                    <hr />

                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/marca"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />

                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default RegistroMarca;