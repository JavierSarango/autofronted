import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
//import { useState } from 'react';
import { GuardarAuto} from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
const RegistroAuto = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    //const [colores, setColores] = useState([]);
    //const [marcas, setMarcas] = useState([]);
    //const [llmarca, setLlmarca] = useState(false);
   // const [llcolor, setLlcolor] = useState(false);
    //acciones
    //submit
    const onSubmit = (data) => {

        var datos = {
            "modelo": data.modelo,
            "anio": data.anio,
            "motor": data.motor,
            "external_marca": data.external_marca,
            "precio": data.precio,
            "color": data.color,
            "placa": data.placa
        };
        GuardarAuto(datos, getToken()).then((info) => {
            
            if (info.error === true) {
                console.log("SI hay error. Lo siento")
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                console.log("No hay error. Suerte campeón!")
                mensajes(info.message);
                navegation('/auto');
            }
        }
        );
    };
    //llamar colores
    // if(!llcolor) {
    //     ObtenerColores().then((info) => {
    //         //  console.log(info);
    //         if (info.error === false) {
    //             //console.log(info.data);
    //             setColores(info.data);
    //         }
    //         setLlcolor(true);
    //     });
    // }
    // if(!llmarca) {
    //     Marcas(getToken()).then((info) => {
    //         //console.log(info);
    //         if(info.error === true && info.message == 'Acceso denegado. Token ha expirado') {
    //             borrarSesion();
    //             mensajes(info.message);
    //             navegation("/sesion");
    //         } else {
    //             setMarcas(info.data);
    //             setLlmarca(true);
    //         }
    //     });
    // }
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de autos!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control form-control-user" placeholder="Ingrese el año" {...register('anio', { required: true })}/>
                                        {errors.anio && errors.anio.type === 'required' && <div className='alert alert-danger'>Ingrese el año del vehículo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" {...register('motor', { required: true })}className="form-control form-control-user" placeholder="Ingrese el motor" />
                                        {errors.motor && errors.motor.type === 'required' && <div className='alert alert-danger'>Ingrese el tipo de motor</div>}
                                    </div>
                                    
                                    <div className="form-group">
                                    <input type="text" {...register('color', { required: true })} className="form-control form-control-user" placeholder="Ingrese un color" />
                                                                                       
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Seleccione un color</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la placa" {...register('placa', { required: true })} />
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'>Ingrese una placa</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio', { required: true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/ })} />
                                        {errors.precio && errors.precio.type === 'required' && <div className='alert alert-danger'>Ingrese el precio</div>}
                                        {errors.precio && errors.precio.type === 'pattern' && <div className='alert alert-danger'>Ingrese un precio valido</div>}

                                    </div>
                                    <div className="form-group">
                                    <input type="text" {...register('external_marca', { required: true })} className="form-control form-control-user" placeholder="Ingrese la marca" />
                                        {errors.external_marca && errors.external_marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}
                                    </div>
                                    <hr />
                                    
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/auto"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />

                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default RegistroAuto;