import '../css/stylea.css';
import '../css/style.css';
import React from 'react';
import { Button } from 'react-bootstrap';
import Header from "./Header";
import Footer from "./Footer";
//import 'bootstrap/dist/css/bootstrap.min.css';
//import RegistroAuto from './RegistroAuto';
import Table from 'react-bootstrap/Table';

import {  ListaMarcas } from '../hooks/Conexion';
import { useEffect, useState } from 'react';
import { getToken } from '../utilidades/Sessionutil';


const ListarMarca = () => {
    
  
    const [data, setData] = useState([]);
  
    

    useEffect(() => {
        const getData = async () => {

            try {
                const response = await ListaMarcas(getToken());
                console.log(response);

                setData(response.info);
            } catch (error) {
                console.log("hola");
                console.error(error);
            }
        };
        getData();

    }, []);



    return (

        <div className="wrapper" >
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className=" text-center">
                            <h1>Marcas Registradas</h1>
                        </div>
                        <div>
                            <Table striped>
                                <thead>
                                    <tr>
                                        <th hidden>#</th>
                                        <th>Nombre</th>
                                        <th>Pais de Origen</th>
                                        <th>Descripcion</th>
                                        <th hidden>External</th>
                                        

                                    </tr>
                                </thead>
                                <tbody>
                                    {data.map((item) => (
                                        <tr key={item.id}>
                                            <td hidden>{item.id}</td>
                                            <td>{item.nombre}</td>
                                            <td>{item.pais_origen}</td>
                                            <td>{item.descripcion}</td>
                                            <td hidden>{item.external_id}</td>
                                            
                                        </tr>
                                    ))}

                                </tbody>
                            </Table>
                            
                        </div>

                    </div>
                </div>
            </div>
            <Footer />
           
        </div>


    );
}

export default ListarMarca;