import '../css/style.css';
import React, { useState } from 'react';
import {  Offcanvas } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
const Canva = () => {
  const [showOffcanvas, setShowOffcanvas] = useState(false);

  const handleToggleOffcanvas = () => {
    setShowOffcanvas(!showOffcanvas);
  };

  return (
    <div>
     
      <button className="btnM" variant="primary" onClick={handleToggleOffcanvas}>
        <span className="icon">
          <svg viewBox="0 0 175 80" width="40" height="40">
            <rect width="80" height="15" fill="#f0f0f0" rx="10"></rect>
            <rect y="30" width="80" height="15" fill="#f0f0f0" rx="10"></rect>
            <rect y="60" width="80" height="15" fill="#f0f0f0" rx="10"></rect>
          </svg>
        </span>
       
      </button>

      <Offcanvas show={showOffcanvas} onHide={handleToggleOffcanvas}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Navegacion</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <Nav.Link href="/Inicio">Inicio</Nav.Link>
          <Nav.Link href="/auto">Autos</Nav.Link>
          <Nav.Link href="/marca">Marcas</Nav.Link>
          <Nav.Link href="/orden">Mantenimiento</Nav.Link>
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  );
};

export default Canva;
