import '../css/stylea.css';
import axios from 'axios';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { GuardarOrden } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
const RegistroOrden = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [ultimoNumeroOrden, setUltimoNumeroOrden] = useState();
    //const [colores, setColores] = useState([]);
    //const [marcas, setMarcas] = useState([]);
    //const [llmarca, setLlmarca] = useState(false);
    // const [llcolor, setLlcolor] = useState(false);
    //acciones
    //submit
    useEffect(() => {
        const obtenerUltimoNumerodeOrden = async () => {
            try {
                const response = await axios.get('http://localhost:3006/api/ordenes/numerodeOrden');
                console.log("esto es response", response.data.nrodeOrden)
                const data = response.data;
                const numeroOrden = data.nrodeOrden;
                const entero = parseInt(numeroOrden, 10);
                const siguienteNumeroOrden = (entero + 1).toString().padStart(numeroOrden.length, '0');
                console.log(siguienteNumeroOrden)
                setUltimoNumeroOrden(siguienteNumeroOrden);
            } catch (error) {
                console.error('Error al obtener el último número de orden:', error);
            }
        };

        obtenerUltimoNumerodeOrden();
    }, []);

    const onSubmit = (data) => {

        var datos = {
            "nroOrden": ultimoNumeroOrden,
            "fecha_ingreso": data.fecha_ingreso,
            "observacion_cliente": data.observacion_cliente,
            "estadoInicialAuto": data.estadoInicialAuto,
            "estado": data.estado,
            "identificacion_persona": data.identificacion_persona,
            "external_auto": data.external_auto
        };
        console.log("datos a enviar", datos)
        GuardarOrden(datos, getToken()).then((info) => {
            console.log("esta es la info", info.error)
            if (info.error === true) {
                console.log("SI hay error. Lo siento")
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                console.log("No hay error. Suerte campeón!")
                mensajes(info.message);
                navegation('/orden');
            }
        }
        );
    };
    //llamar colores
    // if(!llcolor) {
    //     ObtenerColores().then((info) => {
    //         //  console.log(info);
    //         if (info.error === false) {
    //             //console.log(info.data);
    //             setColores(info.data);
    //         }
    //         setLlcolor(true);
    //     });
    // }
    // if(!llmarca) {
    //     Marcas(getToken()).then((info) => {
    //         //console.log(info);
    //         if(info.error === true && info.message == 'Acceso denegado. Token ha expirado') {
    //             borrarSesion();
    //             mensajes(info.message);
    //             navegation("/sesion");
    //         } else {
    //             setMarcas(info.data);
    //             setLlmarca(true);
    //         }
    //     });
    // }
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Orden de Mantenimiento</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>


                                    <div className="form-group">
                                        <input type="date" className="form-control form-control-user" placeholder="Ingrese la fecha de ingreso" {...register('fecha_ingreso', { required: true })} />
                                        {errors.fecha_ingreso && errors.fecha_ingreso.type === 'required' && <div className='alert alert-danger'>Ingrese la fecha de ingreso</div>}
                                    </div>
                                    <div className="form-group">
                                        <textarea {...register('observacion_cliente', { required: true })} className="form-control form-control-user" placeholder="Observaciones del cliente" />
                                        {errors.observacion_cliente && errors.observacion_cliente.type === 'required' && <div className='alert alert-danger'>Observaciones del cliente</div>}
                                    </div>

                                    <div className="form-group">
                                        <textarea {...register('estadoInicialAuto', { required: true })} className="form-control form-control-user" placeholder="Estado Inicial del vehiculo" />

                                        {errors.estadoInicialAuto && errors.estadoInicialAuto.type === 'required' && <div className='alert alert-danger'>Ingrese informacion</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el estado de la orden" {...register('estado', { required: true })} />
                                        {errors.estado && errors.estado.type === 'required' && <div className='alert alert-danger'>Asigne un estado a la orden</div>}
                                    </div>

                                    <div className="form-group">
                                        <input type="text" {...register('identificacion_persona', { required: true })} className="form-control form-control-user" placeholder="Nro de Cédula del cliente" />
                                        {errors.identificacion_persona && errors.identificacion_persona.type === 'required' && <div className='alert alert-danger'>Ingrese un número de cédula valido</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" {...register('external_auto', { required: true })} className="form-control form-control-user" placeholder="Auto" />
                                        {errors.external_auto && errors.external_auto.type === 'required' && <div className='alert alert-danger'>Ingrese info correcta</div>}
                                    </div>

                                    <hr />

                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/orden"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />

                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default RegistroOrden;