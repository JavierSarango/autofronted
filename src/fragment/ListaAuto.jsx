import '../css/stylea.css';
import '../css/style.css';
import React from 'react';
import { Button } from 'react-bootstrap';
import Header from "./Header";
import Footer from "./Footer";
//import 'bootstrap/dist/css/bootstrap.min.css';
//import RegistroAuto from './RegistroAuto';
import Table from 'react-bootstrap/Table';

import { Autos } from '../hooks/Conexion';
import { useEffect, useState } from 'react';
import { getToken } from '../utilidades/Sessionutil';


const ListaAuto = () => {
    // const [show, setShow] = useState(false);

    //const handleClose = () => setShow(false);
    //const handleShow = () => setShow(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        const getData = async () => {

            try {
                const response = await Autos(getToken());
                console.log(response);

                setData(response.info);
            } catch (error) {
                console.log("hola");
                console.error(error);
            }
        };
        getData();

    }, []);

    // const handleModificarAuto = (autoID) => {
    // Aquí se ejecuta la lógica para modificar el auto con el ID proporcionado
    // Puedes llamar a una función o hacer una solicitud HTTP para actualizar los datos del auto

    // Ejemplo de cómo actualizar el color del auto con el ID proporcionado
    // Acción que deseas realizar al hacer clic en la fila

    //setSelectedRow(autoID);
    //setShowForm(true);
    //};

    return (

        <div className="wrapper" >
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="text-gred">

                            <div className="justify-content-center">
                                <h1>Autos Registrados</h1>

                            </div>
                            <div className='d-flex justify-content-end mb-3 me-4'>
                                <Button variant="primary" href='/auto/registro'>
                                    Agregar Autos
                                </Button>

                            </div>
                        </div>
                        <div>
                            <Table striped>
                                <thead>
                                    <tr>
                                        <th hidden>#</th>
                                        <th>Modelo</th>
                                        <th>Color</th>
                                        <th>Motor</th>
                                        <th>Placa</th>
                                        <th>Precio</th>
                                        <th>Año</th>
                                        <th hidden>Propietario</th>
                                        <th hidden>External</th>
                                        <th>Acciones</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {data.map((item) => (
                                        <tr key={item.id}>
                                            <td hidden>{item.id}</td>
                                            <td>{item.modelo}</td>
                                            <td>{item.color}</td>
                                            <td>{item.motor}</td>
                                            <td>{item.placa}</td>
                                            <td>{item.precio}</td>
                                            <td>{item.anio}</td>
                                            <td hidden>{item.propietario}</td>
                                            <td hidden>{item.external_id}</td>
                                            <td>

                                                <a className='btn btn-warning' href='/'>Modificar</a>
                                                <a className='btn btn-primary ms-4' href='/orden/registro'>Mantenimiento</a>
                                            </td>
                                        </tr>
                                    ))}

                                </tbody>
                            </Table>

                        </div>

                    </div>
                </div>
            </div>
            <Footer />

        </div>


    );
}

export default ListaAuto;