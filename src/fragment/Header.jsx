
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { borrarSesion } from '../utilidades/Sessionutil';
import Canva from './Canva';

const Header = () => {
    //const cargarImagen = require.context("../img");
    return (
        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <Navbar bg="light" expand="lg">
                <Container>
                    <div className='d-flex me-3'>
                        <Canva />
                    </div>
                    <Navbar.Brand href="/Inicio">Concesionario </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                   
                </Container>
            </Navbar>


        </nav>
    );
}

export default Header;