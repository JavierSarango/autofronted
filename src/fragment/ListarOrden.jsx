import '../css/stylea.css';
import '../css/style.css';
import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'react-bootstrap';
import Header from "./Header";
import Footer from "./Footer";
//import 'bootstrap/dist/css/bootstrap.min.css';
import mensajes from '../utilidades/Mensajes';
import Table from 'react-bootstrap/Table';
import { useNavigate } from 'react-router-dom';
import { ListaOrdenes, ModificarOrden } from '../hooks/Conexion';
import { useEffect, useState } from 'react';
import { getToken } from '../utilidades/Sessionutil';
import { useForm } from 'react-hook-form';


const ListarOrden = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    const [showModal, setShowModal] = useState(false);
    let [selectedObject, setSelectedObject] = useState(null);
    const [nrodeOrdenes, setNrodeOrdenes] = useState(null);

    const onSubmit = (data) => {

        var datos = {
            "nroOrden": nrodeOrdenes,
            "observacion_cliente": data.observacion_cliente,
            "estadoInicialAuto": data.estadoInicialAuto,
            "estado": data.estado,

        };
        console.log("datos a enviar", selectedObject.nroOrden)
        ModificarOrden(datos, getToken()).then((info) => {
            console.log("esta es la info", info.error)
            if (info.error === true) {
                console.log("SI hay error. Lo siento")
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                console.log("No hay error. Suerte campeón!")
                mensajes(info.message);
                navegation('/orden');
            }
        }
        );
    };
    //const handleShow = () => setShow(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        const getData = async () => {

            try {
                const response = await ListaOrdenes(getToken());
                console.log(response);

                setData(response.info);
            } catch (error) {
                console.log("hola");
                console.error(error);
            }
        };
        getData();

    }, []);

    const handleModificarClick = (itemOrden) => {

        var ObjetoSeleccionado = data.find(item => item.nroOrden === itemOrden);

        console.log('este es item seleccionado', itemOrden)
        console.log('este es bro', ObjetoSeleccionado)       
        setNrodeOrdenes(ObjetoSeleccionado.nroOrden);
        setSelectedObject(ObjetoSeleccionado);
        setShowModal(true);
        console.log('estado show', showModal)
       
        // Aquí puedes agregar lógica adicional que necesites realizar al hacer clic en "Modificar"
    };
    const handleCancelarClick = () => {
        window.location.reload();
        selectedObject = null
        setSelectedObject(selectedObject); // Limpiamos el objeto seleccionado
        setNrodeOrdenes(null); // Limpiamos el número de orden
        setShowModal(false); // Ocultamos el modal
        setSelectedObject(null); 
        console.log('este es cancelar', selectedObject)
        console.log("show cerrado entonces show:", showModal)
    };



    return (

        <div className="wrapper" >
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="text-gred">
                            <div className="d-flex justify-content-center">
                                <h1>Ordenes de Mantenimiento</h1>

                            </div>
                            <div className='d-flex justify-content-end mb-3 mt-2'>
                                <Button variant="primary" href='/orden/registro'>
                                    Nueva Orden de Mantenimiento
                                </Button>
                            </div>
                        </div>
                        <div>
                            <Table striped>
                                <thead>
                                    <tr>
                                        <th hidden>#</th>
                                        <th>Nro. Orden</th>
                                        <th>Fecha Ingreso</th>
                                        <th>Estado Orden</th>
                                        <th>Auto</th>
                                        <th>Cliente</th>
                                        <th hidden>Observacion</th>
                                        <th hidden>External</th>
                                        <th>Acciones</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {data.map((item) => (
                                        <tr key={item.id}>
                                            <td hidden>{item.id}</td>
                                            <td>{item.nroOrden}</td>
                                            <td>{new Date(item.fecha_ingreso).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}</td>
                                            <td>{item.estado}</td>
                                            <td>{item.auto.modelo}</td>
                                            <td>{item.persona.nombres + ' ' + item.persona.apellidos}</td>
                                            <td hidden>{item.propietario}</td>
                                            <td hidden>{item.external_id}</td>
                                            <td>
                                                <a className='btn btn-warning' onClick={() => handleModificarClick(item.nroOrden)}>Modificar</a>
                                                <a className='btn btn-success ms-4' href='/'>Facturar</a>
                                            </td>
                                        </tr>
                                    ))}

                                </tbody>
                            </Table>

                        </div>


                    </div>
                    {showModal && (

                        <Modal show={showModal} onHide={handleCancelarClick}>
                            <ModalHeader>
                                <div>

                                    <h3>Modificar Orden</h3>
                                </div>
                            </ModalHeader>
                            <ModalBody onHide={handleCancelarClick}>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>

                                    <div className="form-group">
                                        <textarea {...register('observacion_cliente', { required: true })} className="form-control form-control-user" placeholder="Observaciones del cliente" defaultValue={selectedObject.observacion_cliente} />
                                        {errors.observacion_cliente && errors.observacion_cliente.type === 'required' && <div className='alert alert-danger'>Observaciones del cliente</div>}
                                    </div>

                                    <div className="form-group">
                                        <textarea {...register('estadoInicialAuto', { required: true })} className="form-control form-control-user" placeholder="Estado Inicial del vehiculo" defaultValue={selectedObject.estadoInicialAuto} />

                                        {errors.estadoInicialAuto && errors.estadoInicialAuto.type === 'required' && <div className='alert alert-danger'>Ingrese informacion</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el estado de la orden" {...register('estado', { required: true })} defaultValue={selectedObject.estado} />
                                        {errors.estado && errors.estado.type === 'required' && <div className='alert alert-danger'>Asigne un estado a la orden</div>}
                                    </div>

                                    <hr />

                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="MODIFICAR"></input>

                                </form>
                            </ModalBody>
                            <ModalFooter>
                                <Button color='danger' onClick={handleCancelarClick}>Cancelar</Button>
                            </ModalFooter>



                        </Modal>

                    )}


                </div>
            </div>
            <Footer />

        </div>


    );
}

export default ListarOrden;