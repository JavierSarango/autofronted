import Sesion  from './fragment/Sesion';
import Principal from './fragment/Principal';
import Inicio from './fragment/Inicio';
import ListaAuto from './fragment/ListaAuto';
import RegistroAuto from './fragment/RegistroAuto';
import {Routes, Route, useLocation, Navigate} from 'react-router-dom';
import { estaSesion } from './utilidades/Sessionutil';
import ModificarAuto from './fragment/ModificarAuto';
import RegistroMarca from './fragment/RegistroMarca';
import ListarMarca from './fragment/ListarMarca';
import ListarOrden  from './fragment/ListarOrden';
import RegistroOrden from './fragment/RegistroOrden';
function App() {

const Middeware = ({children}) => {
  const autenticado = estaSesion();
  const location = useLocation();
  if(autenticado) {
    return children;
  } else {
    return <Navigate to='/sesion' state={location}/>;
  }
}

const MiddewareSession = ({children}) => {
  const autenticado = estaSesion();
 
  if(autenticado) {
    return <Navigate to='/inicio'/>;
  } else {
    return children;
  }
}

  return (
    <div className="App">
      <Routes>
      <Route path='/' element={<MiddewareSession><Principal/></MiddewareSession>} exact/>
      <Route path='/sesion' element={<MiddewareSession><Sesion/></MiddewareSession>}/>
      <Route path='/inicio' element={<Middeware><Inicio/></Middeware>}/>
      <Route path='/auto' element={<Middeware><ListaAuto/></Middeware>}/>
      <Route path='/auto/registro' element={<Middeware><RegistroAuto/></Middeware>}/>
      <Route path='/auto/modificar' element={<Middeware><ModificarAuto/></Middeware>}/>
      <Route path='/marca/registro' element={<Middeware><RegistroMarca/></Middeware>}/>
      <Route path='/marca' element={<Middeware><ListarMarca/></Middeware>}/>
      <Route path='/orden' element={<ListarOrden/>}/>
      <Route path='/orden/registro' element={<Middeware><RegistroOrden/></Middeware>}/>
      </Routes>
      </div>
  );
}

export default App;
